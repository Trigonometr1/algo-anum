% a = diagonal tengah, b = diagonal atas, c = diagonal bawah
% d = vektor hasil (ibaratnya b di Ax=b)
% a, b, c, d adalah vektor 1 kolom
function x = SolveTridiagBanded(a,b,c,d)
  n = length(d);
  for i=2:n
    a(i) = a(i) - (c(i-1)/a(i-1))*b(i-1);
    d(i) = d(i) - (c(i-1)/a(i-1))*d(i-1);
  endfor
  x(n) = d(n)/a(n);
  for i=n-1:-1:1
    x(i) = (d(i)-b(i)*x(i+1))/a(i);
  endfor
