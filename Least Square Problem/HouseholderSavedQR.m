function [QT,R]=HouseholderSavedQR(A) % Matrix Q orthogonal dan Q*A=R
  [m,n]=size(A);
  QT=eye(m);
  for j=1:n
    v=A(j:m,j)+sign(A(j,j))*norm(A(j:m,j))*[1;zeros(m-j,1)];
    a=2/(v'*v);
    Q1=eye(m-j+1)-a*v*v'
    QT=[eye(j-1) zeros(j-1,m-j+1); zeros(m-j+1,j-1) Q1]*QT
    for i=j:n
      A(j:m,i)=A(j:m,i)-a*v'*A(j:m,i)*v;
    end
  end
  R=A;
