function [R,bt] = HouseholderSavedRbt(A,b) % R=QA dan bt = Qb
  [m,n] = size(A);
  A=[A b];
  for j = 1:n
    v = A(j:m,j) + sign(A(j,j))*norm(A(j:m,j))*[1;zeros(m-j,1)];
    coef = 2/(v'*v);
    for i = j:n+1
      A(j:m,i) = A(j:m,i) - (coef*v'*A(j:m,i))*v;
    endfor
  endfor
  R = A(:,1:n);
  bt = A(:,n+1);