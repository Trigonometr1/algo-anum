function [R,bt,p]=QRPivotForDeficiency(A,b)
  [m,n]=size(A);
  C=[A b];p=1:n;
  for j=1:n
    i=1; normcol=0;
      for k=j:n
        normcol(i)=norm(C(j:m,k)); i=i+1;
      end
    [x,k]=max(normcol);
    k=k+j-1;
    C(:,[j k])=C(:,[k j]);
    p([j k])=p([k j]);
    v=C(j:m,j)+sign(C(j,j))*norm(C(j:m,j))*[1;zeros(m-j,1)];
    g=v'*v;
    if g< 10^-12
      break;
     end
    a=2/g;;
    for i=j:n+1
	    C(j:m,i)=C(j:m,i)-a*v'*C(j:m,i)*v;
    end    
  end
  R=C(1:m,1:n);
  bt=C(:,n+1);