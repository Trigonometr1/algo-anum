function [x, counter] = bisection(a,b,tol,counterMax)
  m = (a+b)/2
  counter = 0
  while (b - a > tol) && (counter < counterMax)
    if f(m)*f(a) < 0
      b = m
    else
      a = m
    endif
    counter = counter + 1
    m = (a+b)/2
  endwhile
  x = m