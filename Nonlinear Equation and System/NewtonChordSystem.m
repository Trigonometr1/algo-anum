function [v,counter] = NewtonChordSystem(x, tol, counterMax, m)
  counter = 0;
  F0 = FMatrixGeneral(x)
  while (norm(F0) > tol) && (counter < counterMax)
    J0 = JacobianGeneral(x)
    [L,U] = LUFact(J0)
    for i=1:m
      F0 = FMatrixGeneral(x)
      y = bawah(L, -F0)
      d = atas(U, y)
      x = x + d
      counter = counter + 1
    endfor
  endwhile
  v = x;
