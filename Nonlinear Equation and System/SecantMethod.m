function [x,counter] = SecantMethod(x0, x1, tol,counterMax)
  counter = 0;
  f = @(x) x^x - 2
  while abs(f(x0)) > tol && counter < counterMax
    x2 = x1 - ((f(x1) * (x1-x0))/(f(x1) - f(x0)))
    x0 = x1
    x1 = x2
    counter = counter + 1
  endwhile
  x = x1;