function F = FMatrixGeneral(x)
  n = length(x);
  F = zeros(n,1);
  F(1) = x(1)+x(2)-x(1)*x(2)+2;
  F(2) = x(1)*exp(-x(2))-1;