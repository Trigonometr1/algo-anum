function J = JacobianGeneral(x)
  n = length(x);
  h = 10^-5;
  for i = 1:n
    F_x = FMatrixGeneral(x);
    for j = 1:n
      x_jh = x;
      x_jh(j) = x_jh(j) + h;
      F_ih = FMatrixGeneral(x_jh);
      J(i,j) = (F_ih(i) - F_x(i))/h
    endfor
  endfor
