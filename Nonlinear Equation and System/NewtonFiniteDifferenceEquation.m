function [x,counter] = NewtonFiniteDifferenceEquation(x0, tol, counterMax)
  h = 10^-5;
  counter = 0
  while abs(f(x0)) > tol && counter < counterMax
    x0 = x0 - (f(x0)*h/(f(x0+h) - f(x0)))
    counter = counter + 1
  endwhile
  x = x0;
