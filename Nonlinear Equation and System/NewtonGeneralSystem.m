function [v,counter] = NewtonGeneralSystem(x,tol, counterMax)
  counter = 0
  F0 = FMatrixGeneral(x);
  while (norm(F0) > tol) && (counter < counterMax)
    J = JacobianGeneral(x)
    d = J\(-F0)
    x = x + d
    F0 = FMatrixGeneral(x);
    counter = counter + 1
  endwhile
  v = x