function y = f(x)
  %y = exp(-2*x) - 5*x;
  %y = x^2 - 4*x + 4;
  % rosenberg
  % y = (1-x(1))^2 + 100*(x(2) - x(1)^2)^2;
  % lagrange
  %r = x(1);
  %h = x(2);
  %l = x(3);
  
  %y = pi*r^2 + pi*r*sqrt(r^2 + h^2) + l*(pi*r^2*h/3 - 1);
  y = x^3 - 5*x + 1