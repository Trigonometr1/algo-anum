function H = HessianSecondOrderWithGradien(x)
  n = length(x);
  h = 10^-6;
  
  for j=1:n
    xjp = x;
    xjp(j) += h;
    
    xjm = x;
    xjm(j) -= h;
    
    
    H(:,j) = (GradienSecondOrder(xjp) - GradienSecondOrder(xjm))/(2*h);
  endfor