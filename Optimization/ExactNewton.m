function [x0,n] = ExactNewton(x0, tol, max_iter)
  % sample: newtonSystem([0; 1], 10^-5, 10)
  n = 0;
  F0 = GradienSecondOrder(x0);
  
  while norm(F0) > tol && n < max_iter
    J0 = HessianSecondOrder(x0);
    d = J0\-F0;
    x0 = x0 + d
    
    # update condition
    n = n + 1;
    F0 = GradienSecondOrder(x0);
  endwhile