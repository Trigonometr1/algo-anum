function g=GradienSecondOrder(x)
  h = 10^-5;
  n = length(x);
  g = zeros(n,1);
  for i=1:n
    xih_plus = x; xih_minus = x;
    xih_plus(i) = xih_plus(i) + h;
    xih_minus(i) = xih_minus(i) - h;
    g(i) = (f(xih_plus) - f(xih_minus))/(2*h);
  endfor