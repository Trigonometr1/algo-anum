function [x,k] = ConjugateGradien(x0, tol, max_iter)
  # example run: [x,n] = conjugate_grad([0;0], 10^-4, 1000)
  
  k = 0;
  # iterasi pertama luar loop
  n = length(x0);
  g0 = GradienSecondOrder(x0);
  p0 = -g0;
  a = LineSearchBisection(x0, p0, tol);
  
  x1 = x0 + a*p0; 
  g1 = GradienSecondOrder(x1);
  x0 = x1;
  
  while norm(g1) > tol && k < max_iter
    y = g1 - g0;
    g0 = g1;
    beta = (g1'*y)/(p0'*y);
    p1 = -g1 + beta * p0;
    
    a = LineSearchBisection(x0, p1, tol);
    x1 = x0 + a*p1;
    g1 = GradienSecondOrder(x1);
    
    # Geser utk simpan
    x0 = x1;
    p0 = p1;
    
    k += 1;
  endwhile
  
  x = x1;